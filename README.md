# Powerplay Guide

A small, static HTML web project to host a hastily written guide for the AMDGPU Powerplay sysfs interface.


# Scope

This should serve as an approachable resrouce to introduce AMD GPU users to the methods used to manipulate the clock speeds and voltages of their graphics card, should they want to (for example) overclock for better performance, or undervolt for better thermals and lower power consumption.

There's a suggested route to apply user modifications at boot, though this should be improved upon.

We also very briefly explore third party GUI utilities to achive the same results in a more user-friendly manner.


# Roadmap

Explore the upcoming DPM interface that can (optionally) be used with Radeon VII and Navi.