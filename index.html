<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Powerplay on Linux</title>
		<meta charset="utf-8">
		<meta property="og:title" content="Powerplay on Linux">
		<meta property="og:description" content="A guide on making powerplay work with AMD GPUs on linux.">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="index.css">
	</head>

	<body>
		<nav>
			<a class="ipfs" href="https://ipfs.io/"><h1>Hosted on <img src="images/IPFS_logo.svg" alt="IPFS"></h1></a>
		</nav>
		<div id="content">
			<h1>How to manage voltages &amp; clock speeds for your AMD GPU in Linux</h1>
			<br>

			<h4>Preface &nbsp;&nbsp;&nbsp;<a href="#startGuide">(Skip to instructions)</a></h4>
			<h5 class="quote">"Why do?"</h5>
			<p>
				Your modern AMD GPU (barring Navi) typically operates at quite generous voltages.
				This is applicable to latest iterations of GCN (Polaris 20 & 30, Vega 10 & 20).
				GCN GPUs have never really overclocked well, as you almost immediately hit a 'voltage wall'.
				Your modern AMD GPU (barring Navi) typically operates at quite generous voltages.
				Vega is particularly interesting in this regard, as it can achieve a substantial undervolt - and at the same time - 
				benefit from a memory overclock (which is where you <em>actually</em> see significant performance gains).
			</p>

			<p>
				If you're familiar with Windows, you might be aware of a feature bundled in the AMD driver package called Wattman,
				which allows you to interact with something known as power states on your GCN GPU.
			</p>

			<a href="images/Wattman.png" target="_blank">
				<div class="screenshot first" alt="Screenshot of the Wattman control panel seen within Radeon Settings on Windows"></div>
				<p>
					<small>Wattman - AMD Radeon Settings</small>
				</p>
			</a>

			<p>
				These power states - of which there are 8 in total - determine a clock speed and voltage based on your current graphical workload.
				For example: idling on your desktop should leave you at state 0.
				Playing The Witcher 3 on maxed out settings might throw your GPU into state 6 or 7.
				The above functionality is actually present in the AMDGPU kernel driver, we just need to enable it.
			</p>
			<br>

			<h5 class="quote">"But why do tho?"</h5>
			<p>
				You can get your AMD GPU to run surprisingly lean (in terms of performance per watt) at stock (or <em>higher</em>) perfomance.
			</p>
			<br>

			<h5 class="quote">"Why should I care? Electricity is cheap in my country."</h5>
			<p>
				That's great and all, but the benefits of running leaner are running <em>cooler and quieter</em>.
				If you have a reference cooler, this is something you'd definitely want to look into.
				You'll likely be able to sustain higher clock speeds without the risk of thermal throttling.
				<br>
				Besides that, everyone should strive towards better power efficiency.
			</p>
			<br>

			<h5 class="quote">"Get an Nvidia GPU then."</h5>
			<p>
				I absolutely cannot deny that Nvidia release stellar products, and they've been unbeatable in desktop GPU performance per watt for an absurdly long time now.
				I also cannot deny they're a bit of a lame company for a number of reasons...
			<br><br>
				For nearly every proprietary graphics technology they introduce, AMD produce an open alternative or standard.
				There are also the matters of increasingly prohibitive pricing, controversial business practices, and the lack of an open source Linux driver capable of reclocking modern GPUs.
			</p>
			<br>

			<p>
				In this example, I'll be using Fedora 29, though this should be applicable to any desktop distribution running kernel 4.18 (which is the minimum requirement for Vega 10) or newer.
			</p>
			<br>

			<h4 id="startGuide">Grub</h4>
			<p>
				To get started, you'll want to insert a boot parameter: <code>amdgpu.ppfeaturemask=0xffffffff</code> within <code>/etc/default/grub</code> to <code>GRUB_CMDLINE_LINUX_DEFAULT</code> or <code>GRUB_CMDLINE_LINUX</code>.<br>
				You can temporarily apply this at boot by pressing <code>e</code> on the highlighted kernel to add the parameter for this session only, or you can modify your grub config found at <code>/etc/default/grub</code> and build <code>grub.cfg</code><br>
			<br>

				We'll go with the second option - from your terminal, run: <code>sudo vi /etc/default/grub</code>, press <code>i</code> to enter insert mode, and insert <code>amdgpu.ppfeaturemask=0xffffffff</code> to <code>GRUB_CMDLINE_LINUX</code> (in my case), ensuring that it's within the double quotes:<br>
			</p>
			<a href="images/grub.png" target="_blank">
				<div class="screenshot second" alt="Screenshot taken from terminal showing where to insert the Powerplay featuremask"></div>
				<p>
					<small>Editing /etc/default/grub from the terminal</small>
				</p>
			</a>

			<p>
				Then hit <code>Esc</code>, and then type <code>:wq</code> to write your changes to the file and quit respectively.
				<br>
				Of course you can use whichever text editor you prefer.<br>
				<br>
				Now, you'll want to update your bootloader with the feature mask you've just supplied:<br>
				<br>
				For a BIOS boot system:<br>
				<code>grub2-mkconfig -o /etc/grub2.cfg</code><br>
				<br>

				For a UEFI boot system:<br>
				<code>grub2-mkconfig -o /etc/grub2-efi.cfg</code><br>
				<br>
				Alternatively, you can build these changes with a tool called <code>grubby</code>.<br>
				<br>
				If you don't know what type of system you're running, look for <code>/sys/firmware/efi/</code><br>
				If that's not present, you're not using UEFI. If you're still unsure, go ahead and run both.<br>
				There's no real harm in erroneously building grub for the wrong type of system.<br>
				<br>
				Reboot and can validate your changes by running: <code>cat /sys/module/amdgpu/parameters/ppfeaturemask</code><br>
				This should return the value <code>4294967295</code>; the decimal value of 0 (we set the equivalent hexadecimal value in grub earlier).<br>

			</p>
			<br>

			<h4>Powerplay</h4>
			<p>
				You should now be able to cat the file responsible for storing GPU clock and voltage data:<br>
				<code>cat /sys/class/drm/card0/device/pp_od_clk_voltage</code><br>
				<br>
				It should return the stock core, memory and 'Overdrive' clocks and voltages for your card like so:<br>
			</p>
			<blockquote>
				OD_SCLK:<br>
				0:        852Mhz        800mV<br>
				1:        991Mhz        900mV<br>
				2:       1084Mhz        950mV<br>
				3:       1138Mhz       1000mV<br>
				4:       1200Mhz       1050mV<br>
				5:       1401Mhz       1100mV<br>
				6:       1536Mhz       1150mV<br>
				7:       1630Mhz       1200mV<br>
				OD_MCLK:<br>
				0:        167Mhz        800mV<br>
				1:        500Mhz        800mV<br>
				2:        800Mhz        950mV<br>
				3:        945Mhz       1100mV<br>
				OD_RANGE:<br>
				SCLK:     852MHz       2400MHz<br>
				MCLK:     167MHz       1500MHz<br>
				VDDC:     800mV        1200mV<br>
			</blockquote>

			<p>
				This is your Powerplay table.
			</p>
			<br>

			<h4>Customise your clock speeds and voltages</h4>
			<p>
				The following commands will need to be run as root (<code>sudo su</code>).<br>
				<br>
				To allow for custom clocks and voltages you'll need to set DPM to Manual:<br>
				<code>echo "manual" &gt; /sys/class/drm/card0/device/power_dpm_force_performance_level</code><br>
				<br>
				You can set clock speeds and voltages for power and memory states like so:<br>
				<code>echo</code> &lt;"[s / m] [level] [clock] [voltage]"&gt; to change sclk / mclk's clock and voltage<br>
				<br>
				To commit (apply) your custom values:<br>
				<code>echo "c"</code><br>
				<br>
				To restore all default values:<br>
				<code>echo "r"</code><br>
				<br>
				For example, here's the profile I use for my Sapphire Nitro+ Vega 64:<br>
			</p>

			<blockquote>
				echo "s 0 852 800" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 1 991 900" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 2 1084 910" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 3 1138 930" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 4 1200 960" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 5 1401 965" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 6 1512 975" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 7 1612 1020" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				<br>
				echo "m 0 167 800" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "m 1 500 900" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "m 2 800 910" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "m 3 1050 965" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				<br>
				echo "c" &gt; /sys/class/drm/card0/device/pp_od_clk_voltage<br>
			</blockquote>

			<p>
				At this point, you'll need to set these values after each reboot.<br>
				With that in mind, it might be worth creating a startup script / service to set your preferred Powerplay values on login.
			</p>
			<br>
			<h4>
				Systemd service
			</h4>
			<p>
				Because I'm lazy, I created two script files to apply and reset my Powerplay table, and a systemd service to run the former when I log in.<br>
				<br>
				PLEASE NOTE: The below values are for my Vega 64, and should not be carelessly applied to any other model AMD GPU.<br>
				<br>
				The contents of the shell script files are:
			</p>

			<p class="file-title">ppstart.sh</p>
			<blockquote>
				#!/bin/bash<br>
				echo "manual" > /sys/class/drm/card0/device/power_dpm_force_performance_level<br>
				echo "s 0 852 800" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 1 991 900" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 2 1084 910" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 3 1138 930" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 4 1200 960" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 5 1401 975" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 6 1512 985" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "s 7 1612 1020" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "m 0 167 800" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "m 1 500 900" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "m 2 800 910" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "m 3 1020 975" > /sys/class/drm/card0/device/pp_od_clk_voltage<br>
				echo "c" > /sys/class/drm/card0/device/pp_od_clk_voltage
			</blockquote>

			<p class="file-title">ppstop.sh</p>
			<blockquote>
				#!/bin/bash<br>
				echo "r" > /sys/class/drm/card0/device/pp_od_clk_voltage
			</blockquote>

			<p>
				Create these (as root) in <code>/opt/powerplay/</code><br>
				Make them executable by running: <code>sudo chmod a+rx ppstart.sh ppstop.sh</code><br>
				You can name them however you wish and place them wherever as long as you refer to them accordingly in the step below.<br>
				<br>
				Next, you can create a service (also via sudo) within <code>/etc/systemd/system/</code> to allow it to run at startup:<br>
			</p>

			<p class="file-title">pptable.service</p>
			<blockquote>
				[Unit]<br>
				Description=Manage GPU performance profiles<br>
				<br>
				[Service]<br>
				Type=oneshot<br>
				ExecStart=/opt/powerplay/ppstart.sh<br>
				RemainAfterExit=true<br>
				ExecStop=/opt/powerplay/ppstop.sh<br>	box-sizing: border-box;
				StandardOutput=journal<br>
				<br>
				[Install]<br>
				WantedBy=multi-user.target
			</blockquote>

			<p>
				With that in place, run: <code>sudo systemctl enable pptable.service && systemctl start pptable.service</code><br>
				This will enable the service to run at startup and immediately apply your custom Powerplay table respectively.<br>
			</p>
			<br>

			<h5 class="quote">"But I want something more like Wattman on Windows!"</h5>
			<p>
				Well you're in luck!<br>
			</br>
				There's a number of graphical front ends that interact with the Powerplay sysfs interface.<br>
				<br>
				I would highly recommend looking into <a href="https://gitlab.com/corectrl/corectrl" target="_blank">CoreCtrl</a> (QT 5) and
				<a href="https://github.com/BoukeHaarsma23/WattmanGTK" target="_blank">WattmanGTK</a> (GTK 3).
			</p>

			<a href="images/CoreCtrl.png" target="_blank">
				<div class="screenshot third" alt="Screenshot of CoreCtrl running on Fedora"></div>
				<p>
					<small>The CoreCtrl UI</small>
				</p>
			</a>

			<br>

			<p>
				Thank you for reading - we hope this has been helpful for you.
			</p>

			<h4>Contributors:</h4>

			<a href="https://gitlab.com/Vikingtons" class="contrib-link" target="_blank">
				<div class="contrib-element">
					<div class="contrib-avatar vik"></div>
					<p class="contrib-name">Vikingtons</p>
				</div>
			</a>

			<a href="https://www.swedneck.xyz/" class="contrib-link" target="_blank">
				<div class="contrib-element">
					<div class="contrib-avatar swed"></div>
					<p class="contrib-name">Swedneck</p>
				</div>
			</a>

			<h4>Want to make this guide better?</h4>

			<a href="https://gitlab.com/Vikingtons/powerplay-guide"  class="contrib-link" target="_blank">
				<div class="contrib-element clone">
					<div class="contrib-avatar git"></div>
					<p class="contrib-name">Clone me!</p>
				</div>
			</a>

		</div>

	</body>
</html>
